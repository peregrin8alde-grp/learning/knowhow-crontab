# Jinja2 活用

python のテンプレートエンジンである [Jinja2](https://palletsprojects.com/p/jinja/) を使うことで出力をテンプレート化

```
mkdir lib
pip install Jinja2 -t lib

export PYTHONPATH=lib
```

```
python sample/tmpl2txt.py ./sample config2.json.j2 sample/config.json > sample/config2.json

python sample/tmpl2txt.py ./sample sample.txt.j2 sample/config2.json
```
