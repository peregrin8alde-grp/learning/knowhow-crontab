# -*- coding: utf-8 -*-
import sys
from jinja2 import Environment, FileSystemLoader
import json

args = sys.argv

env = Environment(
    loader=FileSystemLoader(args[1])
)

template = env.get_template(args[2])

json_file = open(args[3], 'r')
json = json.load(json_file)

print(template.render(json))
