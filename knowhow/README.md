# 各種ノウハウ

## 特定の日付の場合だけ処理を変える

日付リストを記述したテキストファイルを参照し、現在日時がそこに含まれる日付の場合だけコマンドを実行する方法。

以下のように、リストに現在日が含まれてるかどうかでコマンド実行有無を分ける。

```
# 特定日のみ実行
grep `date "+%Y/%-m/%-d"`, run.csv > /dev/null && echo aa

# 特定日のみスキップ
grep `date "+%Y/%-m/%-d"`, notrun.csv > /dev/null || echo aa
```

例）祝日だけ除外／実行

[日本の祝日データ](https://www8.cao.go.jp/chosei/shukujitsu/gaiyou.html)の取得

`SSL routines:tls_process_ske_dhe:dh key too small` を回避するために `--ciphers` で SHA の鍵が短いものを指定

```
curl --ciphers AES128-SHA -O https://www8.cao.go.jp/chosei/shukujitsu/syukujitsu.csv
```

```
grep `date "+%Y/%-m/%-d"`, syukujitsu.csv > /dev/null && echo aa

grep `date "+%Y/%-m/%-d"`, syukujitsu.csv > /dev/null || echo aa
```

## エディタを使わず定義編集

```
# 新規作成
tee cronconf.txt << 'EOF'
1 2 3 4 5 touch ~/right_$(date +\%Y\%m\%d).txt

EOF

crontab cronconf.txt

# 追加
crontab -l > cronconf2.txt
tee -a cronconf2.txt << 'EOF'
1 2 3 4 5 touch ~/right_$(date +\%Y\%m\%d).txt >/dev/null 2>&1

EOF

crontab cronconf2.txt
```
