#!/bin/bash

#set -x

topMember=$(sed -n 1p member.txt)

export curMember=${curMember:-${topMember}}

maxIdx=$(wc -l < member.txt)  # 改行の数で判断するので、最終行の改行必須
curIdx=$(grep ${curMember} -n member.txt | cut -d ":" -f 1)

if [ ${curIdx} -eq ${maxIdx} ]; then
  nextIdx=1
else
  nextIdx=$((curIdx + 1))
fi

nextMember=$(sed -n ${nextIdx}p member.txt)

export curMember=${nextMember}

exit 0
